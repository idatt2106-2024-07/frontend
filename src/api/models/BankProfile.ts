/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Account } from './Account';
export type BankProfile = {
    id?: number;
    ssn?: number;
    accounts?: Array<Account>;
};

