/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type BalanceDTO = {
    bban?: number;
    balance?: number;
};

