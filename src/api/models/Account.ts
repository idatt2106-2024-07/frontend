/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BankProfile } from './BankProfile';
export type Account = {
    bban?: number;
    balance?: number;
    bankProfile?: BankProfile;
};

