/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type AuthenticationResponse = {
    firstName?: string;
    lastName?: string;
    userId?: number;
    profileImage?: number;
    role?: string;
    subscriptionLevel?: string;
    token?: string;
};

