/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { UserDTO } from './UserDTO';
export type LeaderboardEntryDTO = {
    user?: UserDTO;
    score?: number;
    rank?: number;
};

