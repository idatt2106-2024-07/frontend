/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type StreakDTO = {
    currentStreak?: number;
    currentStreakCreatedAt?: string;
    currentStreakUpdatedAt?: string;
    highestStreak?: number;
    highestStreakCreatedAt?: string;
    highestStreakEndedAt?: string;
};

