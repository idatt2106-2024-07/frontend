/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type BadgeDTO = {
    id?: number;
    badgeName?: string;
    criteria?: number;
    imageId?: number;
};

