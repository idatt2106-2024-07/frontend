/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ConfigurationDTO } from './ConfigurationDTO';
export type UserUpdateDTO = {
    firstName?: string;
    lastName?: string;
    email?: string;
    profileImage?: number;
    bannerImage?: number;
    savingsAccountBBAN?: number;
    checkingAccountBBAN?: number;
    configuration?: ConfigurationDTO;
};

