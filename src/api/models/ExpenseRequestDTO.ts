/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type ExpenseRequestDTO = {
    expenseId?: number;
    description?: string;
    amount?: number;
};

