/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { LeaderboardEntryDTO } from './LeaderboardEntryDTO';
export type LeaderboardDTO = {
    type?: string;
    filter?: string;
    entries?: Array<LeaderboardEntryDTO>;
};

