/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type BudgetRequestDTO = {
    budgetName?: string;
    budgetAmount?: number;
    expenseAmount?: number;
};

