/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type ProgressDTO = {
    id?: number;
    day?: number;
    amount?: number;
    completedAt?: string;
};

