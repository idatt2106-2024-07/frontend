/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type ConfigurationDTO = {
    commitment?: string;
    experience?: string;
    challengeTypes?: Array<string>;
};

