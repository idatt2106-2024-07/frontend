/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type ItemDTO = {
    id?: number;
    itemName?: string;
    price?: number;
    imageId?: number;
    alreadyBought?: boolean;
};

