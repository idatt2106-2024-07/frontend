/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type FeedbackResponseDTO = {
    id?: string;
    email?: string;
    message?: string;
    createdAt?: string;
};

