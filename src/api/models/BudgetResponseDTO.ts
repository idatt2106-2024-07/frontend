/* generated using openapi-typescript-codegen -- do not edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export type BudgetResponseDTO = {
    id?: number;
    budgetName?: string;
    budgetAmount?: number;
    expenseAmount?: number;
    createdAt?: string;
};

